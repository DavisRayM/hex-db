/// Cell structure definition
///
/// Cells are the main component of a "page" within this database. Cells containing
/// the useful information necessary to maintain and traverse the constructed
/// B+ Tree
use std::{error::Error, io::Write, mem::size_of, usize};

use super::cell_type::CellType;
use crate::ConversionError;

const CELL_TYPE_OFFSET: usize = 0;
const CELL_KEY_SIZE_OFFSET: usize = 1;
const CELL_KEY_META_SIZE: usize = size_of::<usize>();
const CELL_KEY_OFFSET: usize = 1;
const CELL_KEY_SIZE: usize = size_of::<usize>();
const CELL_PAGE_ID_SIZE: usize = size_of::<usize>();
const CELL_VALUE_SIZE: usize = size_of::<usize>();
const BASE_CELL_SIZE: usize = 1 + CELL_KEY_SIZE + CELL_VALUE_SIZE;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Key(pub Vec<u8>);

#[derive(Debug, Clone)]
pub struct Cell {
    pub cell_type: CellType,
    pub key: Key,
}

impl Cell {
    pub fn len(&self) -> usize {
        match self.cell_type {
            CellType::Value(ref bytes) => BASE_CELL_SIZE + bytes.len(),
            CellType::Key(_) => BASE_CELL_SIZE,
        }
    }

    pub fn write_bytes<T: Write>(&self, writer: &mut T) -> Result<(), Box<dyn Error>> {
        let cell_type = &self.cell_type;
        let Key(key) = &self.key;
        writer.write_all(&[cell_type.into()])?;

        let key_size = key.len().to_be_bytes();
        writer.write_all(&key_size)?;
        writer.write_all(key)?;
        match self.cell_type {
            CellType::Key(page_id) => writer.write_all(&page_id.to_be_bytes())?,
            CellType::Value(ref bytes) => {
                writer.write_all(&bytes.len().to_be_bytes())?;
                writer.write_all(&bytes[..])?;
            }
        }

        Ok(())
    }
}

// Error-prone conversion between a byte array into a cell
impl TryFrom<&[u8]> for Cell {
    type Error = ConversionError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let mut cell_type = CellType::try_from(value[CELL_TYPE_OFFSET])?;
        let mut offset: usize = CELL_KEY_SIZE_OFFSET + CELL_KEY_META_SIZE;
        let key_size: usize = usize::from_be_bytes(
            value[CELL_KEY_SIZE_OFFSET..offset]
                .try_into()
                .map_err(|_| ConversionError::new("failed to retrieve key_size".to_string()))?,
        );

        let mut key: Vec<u8> = vec![0; key_size];
        key.clone_from_slice(&value[offset..offset + key_size]);
        offset += key_size;

        match cell_type {
            CellType::Key(ref mut _page_id) => {
                *_page_id = usize::from_be_bytes(
                    value[offset..offset + CELL_PAGE_ID_SIZE]
                        .try_into()
                        .map_err(|_| {
                            ConversionError::new("failed to convert bytes into page ID".to_string())
                        })?,
                );
            }
            CellType::Value(ref mut _bytes) => {
                let bytes_len = usize::from_be_bytes(
                    value[offset..offset + CELL_VALUE_SIZE]
                        .try_into()
                        .map_err(|_| {
                            ConversionError::new(
                                "failed to convert bytes into value size!".to_string(),
                            )
                        })?,
                );

                offset += CELL_VALUE_SIZE;
                for byte in value[offset..offset + bytes_len].iter() {
                    _bytes.push(*byte);
                }
            }
        }

        Ok(Self {
            cell_type,
            key: Key(key),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn key_cell_from_to_bytes() {
        let source: Vec<u8> = vec![
            0x01, // Cell type (Key)
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, // Key size: 1
            0x02, // Key
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1B, // (Page ID: 27)
        ];

        let res = Cell::try_from(&source[..]).unwrap();
        assert_eq!(res.cell_type, CellType::Key(27));
        assert_eq!(res.key.0, vec![0x02]);

        let mut res_vec = Vec::new();
        res.write_bytes(&mut res_vec).unwrap();
        assert_eq!(res_vec, source);
    }

    #[test]
    fn value_cell_from_to_bytes() {
        let source: Vec<u8> = vec![
            0x02, // Cell type (Value)
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, // Key size: 1
            0x02, // Key
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, // (Value size: 3)
            0x1B, 0x1A, 0x10, // Actual value
        ];

        let res = Cell::try_from(&source[..]).unwrap();
        assert_eq!(res.cell_type, CellType::Value(vec![27, 26, 16]));
        assert_eq!(res.key.0, vec![0x02]);

        let mut res_vec = Vec::new();
        res.write_bytes(&mut res_vec).unwrap();
        assert_eq!(res_vec, source);
    }
}
