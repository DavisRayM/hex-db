use crate::ConversionError;
/// Types of possible cells within the page structure
///
/// Cells can be one of two types; A key cell or Key-Value cell. The cells
/// can not/should not co-exist on the same "page"

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum CellType {
    /// Key cells store page ID links
    Key(usize),

    /// Value cells store the raw bytes stored
    Value(Vec<u8>),
}

impl TryFrom<u8> for CellType {
    type Error = ConversionError;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x01 => Ok(CellType::Key(0)),
            0x02 => Ok(CellType::Value(Vec::new())),
            _ => Err(ConversionError::new(format!(
                "unknown cell type {:?}",
                value
            ))),
        }
    }
}

impl From<&CellType> for u8 {
    fn from(value: &CellType) -> u8 {
        match value {
            CellType::Key(_) => 0x01,
            CellType::Value(_) => 0x02,
        }
    }
}
