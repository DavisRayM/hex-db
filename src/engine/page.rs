use std::mem::size_of;

use super::cell::{Cell, Key};
use crate::{engine::cell_type::CellType, ConversionError};

const PAGE_SIZE: usize = 4096;
const PAGE_TYPE_OFFSET: usize = 0;
const PAGE_IS_ROOT_OFFSET: usize = 1;
// TODO: at some point the pages should have a checksum
// const PAGE_CHECKSUM_OFFSET: usize = 2;
const PAGE_NUM_CELL_OFFSET: usize = 2;
const PAGE_NUM_CELL_SIZE: usize = size_of::<usize>();
const PAGE_SIZE_OFFSET: usize = 3;
const PAGE_SIZE_META_SIZE: usize = size_of::<usize>();

pub struct Page {
    pub page_type: PageType,
    pub is_root: bool,
    pub index: Vec<PageIndex>,
}

pub struct PageOffset(pub usize);

pub struct PageIndex(pub Key, pub PageOffset);

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PageType {
    Index,
    Leaf(Vec<u8>),
}

impl From<&PageType> for u8 {
    fn from(value: &PageType) -> Self {
        match value {
            PageType::Index => 0x01,
            PageType::Leaf(_) => 0x02,
        }
    }
}

impl Into<[u8; PAGE_SIZE]> for Page {
    fn into(self) -> [u8; PAGE_SIZE] {
        let mut res = [0; PAGE_SIZE];

        res[PAGE_TYPE_OFFSET] = u8::from(&self.page_type);
        res[PAGE_IS_ROOT_OFFSET] = match self.is_root {
            true => 0x01,
            _ => 0x02,
        };
        res[PAGE_NUM_CELL_OFFSET..PAGE_NUM_CELL_OFFSET + PAGE_NUM_CELL_SIZE]
            .clone_from_slice(&self.index.len().to_be_bytes());
        let mut offset = PAGE_NUM_CELL_OFFSET + PAGE_NUM_CELL_SIZE;

        self.index
            .iter()
            .map(|p| Cell {
                cell_type: CellType::Key(p.1 .0),
                key: Key(p.0 .0.clone()),
            })
            .for_each(|c| {
                let mut buf = Vec::new();

                c.write_bytes(&mut buf)
                    .expect("failed to convert index cell!");

                let buf_size = buf.len();
                res[offset..offset + buf_size].clone_from_slice(&buf[0..buf_size]);
                offset += buf_size;
            });

        match self.page_type {
            PageType::Leaf(ref curr) => {
                for (to, from) in res.iter_mut().zip(curr.iter()) {
                    if *to == 0 && *from != 0 {
                        *to = *from;
                    } else if *from != 0 {
                        panic!("index overflow!");
                    }
                }
            }
            PageType::Index => (),
        }

        res
    }
}

impl TryFrom<u8> for PageType {
    type Error = ConversionError;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x01 => Ok(PageType::Index),
            0x02 => Ok(PageType::Leaf(Vec::new())),
            _ => Err(ConversionError::new("unknown page type!".to_string())),
        }
    }
}

impl TryFrom<[u8; PAGE_SIZE]> for Page {
    type Error = ConversionError;

    fn try_from(value: [u8; PAGE_SIZE]) -> Result<Self, Self::Error> {
        let mut page_type: PageType = value[PAGE_TYPE_OFFSET].try_into()?;
        let is_root = matches!(value[PAGE_IS_ROOT_OFFSET], 0x01);

        let num_cells = usize::from_be_bytes(
            value[PAGE_NUM_CELL_OFFSET..PAGE_NUM_CELL_OFFSET + PAGE_NUM_CELL_SIZE]
                .try_into()
                .map_err(|_| {
                    ConversionError::new("failed to retrieve number of cells in page!".to_string())
                })?,
        );

        let mut offset = PAGE_NUM_CELL_OFFSET + PAGE_NUM_CELL_SIZE;
        let mut index: Vec<PageIndex> = Vec::with_capacity(num_cells);

        for _ in 0..num_cells {
            let cell = Cell::try_from(&value[offset..])?;
            offset += cell.len();

            match cell.cell_type {
                CellType::Key(offset) => {
                    index.push(PageIndex(cell.key, PageOffset(offset)));
                }
                CellType::Value(_) => {
                    eprintln!("page format mismatch");
                    return Err(ConversionError::new("page format mismatch!".to_string()));
                }
            }
        }

        match page_type {
            PageType::Leaf(ref mut data) => {
                data.clone_from_slice(&value[..]);
            }
            PageType::Index => (),
        }

        Ok(Page {
            page_type,
            is_root,
            index,
        })
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn page_conversion_to_from_bytes() {
        const PAGE_HEADER_LEN: usize = 1 + 1 + PAGE_NUM_CELL_SIZE;
        let source: [u8; PAGE_HEADER_LEN] = [
            0x01, // Index page
            0x01, // Is root
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, // Number of cells (1)
        ];
        let cell_data: [u8; 18] = [
            0x01, // Cell type (Key)
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, // Key size: 1
            0x02, // Key
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1B, // (Page ID: 27)
        ];
        let junk_data: [u8; PAGE_SIZE - (PAGE_HEADER_LEN + 18)] =
            [0x00; PAGE_SIZE - (PAGE_HEADER_LEN + 18)];

        let mut page = [0x00; PAGE_SIZE];

        for (to, from) in page.iter_mut().zip(
            source
                .iter()
                .chain(cell_data.iter())
                .chain(junk_data.iter()),
        ) {
            *to = *from;
        }

        let res = Page::try_from(page).unwrap();
        assert_eq!(res.page_type, PageType::Index);
        assert!(res.is_root);
        assert_eq!(res.index.len(), 1);
        assert_eq!(res.index[0].0 .0, vec![0x02]);
        assert_eq!(res.index[0].1 .0, 27);

        let res: [u8; PAGE_SIZE] = res.into();
        assert_eq!(res, page)
    }
}
