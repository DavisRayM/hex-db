use std::fmt;

#[derive(Clone, Debug)]
pub struct ConversionError {
    msg: String,
}

impl fmt::Display for ConversionError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "conversion error: {}", self.msg)
    }
}

impl ConversionError {
    pub fn new(message: String) -> Self {
        Self { msg: message }
    }
}
